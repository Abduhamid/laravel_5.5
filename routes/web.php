<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::group(['prefix'=>'admin'], function () {


Route::get('/there', function () {
//    session(['status' => 'value']);
//session()->flush();
    return redirect('id')->with('status', 'hello world');
})->name('there');
Route::get('/id', 'SiteController@index');
//Route::get('/', '');

//Route::group(['before' => 'auth'], function () {
//
//});
//});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
